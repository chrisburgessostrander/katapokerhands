/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sciotoagritech.katapokerhands;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Chris
 */
public class Card {
    private int rank, suit;
    
    private static String[] suits = 
    	{"Hearts", 		//0
    		"Spades", 	//1
    		"Diamonds", //2
    		"Clubs" };	//3
    private static String[] ranks = 
    	{
   			"2", 		//0
   			"3", 		//1
   			"4", 		//2
    		"5", 		//3
    		"6", 		//4
    		"7",		//5
    		"8",   		//6
   			"9",		//7
   			"10", 		//8
    		"Jack", 	//9
    		"Queen",	//10
    		"King",		//11
    		"Ace"};		//12
    
    public static String suitToString(int suit)
    {
        return suits[suit];
    }
    
    public static int StringToSuit(String s)
    {
    	List<String> allSuits = Arrays.asList(suits); 
    	return allSuits.indexOf(s);
    }
    
    public static String rankToString(int rank)
    {
        return ranks[rank];
    }
    
    public static int StringToRank(String s)
    {
    	List<String> allRanks = Arrays.asList(ranks); 
    	return allRanks.indexOf(s);
    }
    
    Card(int suit, int rank){
        this.rank = rank;
        this.suit = suit;
    }//Card(int suit, int rank)
    
    public String rankOfSuit()
    {
        return ranks[rank] + " of " + suits[suit];
    }//public String rankOfSuit()
    
    public int getRank(){
        return rank;
    }//public int getRank()
    
    public int getSuit(){
        return suit;
    }//public int getSuit()    
    
    public static String[] showRanks(){
        return ranks;
    }//public static String[] showRanks()
    
    public static String[] showSuits(){
        return suits;
    }//public static String[] showSuits()
}//public class Card