/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sciotoagritech.katapokerhands;

/**
 *
 * @author Chris
 */
public class Main {
    
    public static void main(String[] args)
    {
    	runAcceptanceTests();
    	Deck deck= new Deck();
    	System.out.println("Black");
    	Hand black= new Hand(deck);
     	System.out.println("");
    	System.out.println("White");
    	Hand white= new Hand(deck);
     	System.out.println("");
    	System.out.println(Hand.compareHands(black, white));
       
    }// public static void main(String[] args)


    private static void runAcceptanceTests() {
    	System.out.println("Test Case: Black: 2H 3D 5S 9C KD  "
 			+ "White: 2C 3H 4S 8C AH. "
 			+ "Expect White Wins with High Card Ace.");
 	Card[] blackTestCards = new Card[] {
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")),
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("5")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("9")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("King"))};
 	Card[] whiteTestCards = new Card[] {
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("4")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("8")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("Ace"))};
 	Hand testBlack = new Hand(blackTestCards);
 	Hand testWhite = new Hand(whiteTestCards);
 	System.out.println("Test Run 1: " + Hand.compareHands(testBlack, testWhite));
 	System.out.println("");
 	System.out.println("");

 	System.out.println("Test Case: Black: 2H 4S 4C 2D 4H  "
 			+ "White: 2S 8S AS QS 3S. "
 			+ "Expect white wins with Flush.");
 	blackTestCards = new Card[] {
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("4")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("4")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("4"))};
 	whiteTestCards = new Card[] {
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("8")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("Ace")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("Queen")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("3"))};
 	testBlack = new Hand(blackTestCards);
 	testWhite = new Hand(whiteTestCards);
 	System.out.println("Test Run 2: " + Hand.compareHands(testBlack, testWhite));
 	System.out.println("");
 	System.out.println("");

 	System.out.println("Test Case: Black: 2H 3D 5S 9C KD  "
 			+ "White: 2C 3H 4S 8C KH. "
 			+ "Expect Black wins with High Card 9");
 	blackTestCards = new Card[] {
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("5")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("9")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("King"))};
 	whiteTestCards = new Card[] {
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("4")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("8")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("King"))};
 	testBlack = new Hand(blackTestCards);
 	testWhite = new Hand(whiteTestCards);
 	System.out.println("Test Run 3: " + Hand.compareHands(testBlack, testWhite));
 	System.out.println("");
 	System.out.println("");

 	System.out.println("Test Case: Black: 2H 3D 5S 9C KD  "
 			+ "White: 2D 3H 5C 9S KH. "
 			+ "Expect Draw.");
 	blackTestCards = new Card[] {
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("5")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("9")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("King"))};
 	whiteTestCards = new Card[] {
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("5")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("9")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("King"))};
 	testBlack = new Hand(blackTestCards);
 	testWhite = new Hand(whiteTestCards);
 	System.out.println("Test Run 4: " + Hand.compareHands(testBlack, testWhite));
 	System.out.println("");
 	System.out.println("");

 	System.out.println("Test Case: Black: 2H 3D 5S 9C KD  "
 			+ "White: AD 2H 3C 4S 5H. "
 			+ "Expect White wins with Straight.");
 	blackTestCards = new Card[] {
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("5")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("9")), 
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("King"))};
 	whiteTestCards = new Card[] {
 			new Card(Card.StringToSuit("Diamonds"),Card.StringToRank("Ace")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("2")), 
 			new Card(Card.StringToSuit("Clubs"),Card.StringToRank("3")), 
 			new Card(Card.StringToSuit("Spades"),Card.StringToRank("4")), 
 			new Card(Card.StringToSuit("Hearts"),Card.StringToRank("5"))};
 	testBlack = new Hand(blackTestCards);
 	testWhite = new Hand(whiteTestCards);
 	System.out.println("Test Run 5: " + Hand.compareHands(testBlack, testWhite));
 	System.out.println("");
 	System.out.println("");

    }//private static void runAcceptanceTests() 
    
}//public class Main

