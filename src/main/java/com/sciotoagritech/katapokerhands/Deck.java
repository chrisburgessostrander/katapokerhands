/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sciotoagritech.katapokerhands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author Chris
 */

public class Deck {
   private ArrayList<Card> cards;
   
   Deck()
   {
       cards = new ArrayList<Card>();
       //int index_A, index_B;
       //Random generator = new Random();
       for(int suit = 0; suit < Card.showSuits().length; suit++){
           for(int rank = 0; rank < Card.showRanks().length; rank++){
               Card tempCard = new Card(suit,rank);
               cards.add(tempCard);
               System.out.println("Generated " + tempCard.rankOfSuit());
           }
       }
       Collections.shuffle(cards);
       System.out.println("There are " + getTotalCards() + " cards in the deck.");
       System.out.println("");
       System.out.println("");

   }//Deck()
   
   public Card draw()
   {
       return cards.remove(0);
   }//public Card draw()
   
   private int getTotalCards()
   {
       return cards.size();
   }//private int getTotalCards()
}//public class Deck
