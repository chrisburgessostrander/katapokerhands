/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sciotoagritech.katapokerhands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Chris
 */
public class Hand {
    private static String[] hands = 
    	{"High Card", 			//0
    		"Pair", 			//1
    		"Two Pairs", 		//2
    		"Three of a Kind", 	//3
    		"Straight",			//4
    		"Flush", 			//5
    		"Full House", 		//6
    		"Four of a Kind", 	//7
    		"Straight Flush"};	//8
    
	private Card[] cards;
    private static int handSize = 5;
    private Map<String, Card> rankMap = new HashMap<String,Card>(); //Track the ranking card per grading method
    
    public static String[] showHands()
    {
    	return hands;
    }
    
    protected static String compareHands(Hand hand1, Hand hand2) {
	for(int n = Hand.showHands().length - 1; n >= 0; n--)
	{
		   /*{"High Card", 			//0
			"Pair", 				//1
			"Two Pairs", 			//2
			"Three of a Kind", 		//3
			"Straight",				//4
			"Flush", 				//5
			"Full House", 			//6
			"Four of a Kind", 		//7
			"Straight Flush"};		//8*/
		   int hand1Rank = -1;
		   int hand2Rank = -1;
		   String condition = "";
		   switch(n)	
		   {
		   case 8: //Straight Flush
			   	condition = "Straight Flush";
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
		   case 7: //Four of a Kind
			   	condition = "Four of a Kind";
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
			   	break;
		   case 6: //Full House
			   	condition = "Full House";	
			   	hand1Rank = hand1.showMappedRank(condition + " Triplet").getRank();
			   	hand2Rank = hand2.showMappedRank(condition + " Triplet").getRank();
	
				if(hand1Rank == hand2Rank) {
				   	hand1Rank = hand1.showMappedRank(condition + " Double").getRank();
				   	hand2Rank = hand2.showMappedRank(condition + " Double").getRank();
	
				}
				break;
		   case 5: //Flush
				condition = "Flush";
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
			   	break;
		   case 4: //Straight
			   	condition = "Straight";	
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
				break;
		   case 3: //Three of a Kind
			   	condition = "Three of a Kind";
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
				
			   	break;
		   case 2: //Two Pairs
			   	condition = "Two Pairs";
			   	hand1Rank = hand1.showMappedRank(condition + " High").getRank();
			   	hand2Rank = hand2.showMappedRank(condition + " High").getRank();
			   	
				if(hand1Rank == hand2Rank) {
				   	hand1Rank = hand1.showMappedRank(condition + " Low").getRank();
				   	hand2Rank = hand2.showMappedRank(condition + " Low").getRank();
	
				}
				break;
		   case 1: //Pair
			   	condition = "Pair";
			   	hand1Rank = hand1.showMappedRank(condition).getRank();
			   	hand2Rank = hand2.showMappedRank(condition).getRank();
			   	break;
		   default: //High Card
			   condition = "High Card";
			   	for(int j = 4; j >= 0; j--)//5 cards in hand ordered least to highest
			   	{
			   		hand1Rank = hand1.getCard(j).getRank();
			   		hand2Rank = hand2.getCard(j).getRank();
			   		if(hand1Rank != hand2Rank) {
			   			break;
			   		}
			   	}
		   	}
		   condition = condition + " with ";
		   if(hand1Rank == hand2Rank) {
			   	continue;
		   }else if(hand1Rank > hand2Rank) {
			   	return "Black Wins with " + condition + Card.rankToString(hand1Rank);
		   }else return "White Wins " + condition + Card.rankToString(hand2Rank);
		}
		return 	"It's a draw!";
	}//private static String compareHands(Hand hand1, Hand hand2) 
    
    Hand(Deck deck){
        cards = new Card[handSize];
        for (int n = 0; n < handSize; n++)
        {
            cards[n] = deck.draw();
        }

        //Bubble Sort for easier analysis.
        for(int x = 0; x < cards.length - 1; x++) {
        	for(int y = 0; y < cards.length-1-x; y++) //-x to prevent unnecessary passes.
        	{
        		if(cards[y].getRank() > cards[y+1].getRank())
        		{
        			Card tempCard = cards[y];
        			cards[y] = cards[y+1];
        			cards[y+1] = tempCard;
        		}
        	}
        }
        
        gradeHand();
    }// Hand(Deck deck)
    
    Hand(Card[] cardArray){
    	cards = cardArray;
    	 //Bubble Sort for easier analysis.
        for(int x = 0; x < cards.length - 1; x++) {
        	for(int y = 0; y < cards.length-1-x; y++) //-x to prevent unnecessary passes.
        	{
        		if(cards[y].getRank() > cards[y+1].getRank())
        		{
        			Card tempCard = cards[y];
        			cards[y] = cards[y+1];
        			cards[y+1] = tempCard;
        		}
        	}
        }
        
        gradeHand();
    }//Hand(Card[] cardArray)
    
    public Card showMappedRank(String s)
    {
    	return rankMap.get(s);
    }//public Card showMappedRank(String s)
    
    public Card[] getCards()
    {
    	return cards;
    }//public Card[] getCards()
    public Card getCard(int n)
    {
    	return cards[n];
    }// public Card getCard(int n)
    
    private boolean flush()
    {
        //traverse through the cards; if one card's suit doesn't match the suit of the next card, there' no flush.
        for(int n = 0; n < cards.length - 1; n++)
        {
            if(cards[n].getSuit() != cards[n+1].getSuit())
            {
                return false;
            }
        }
        return true;
    }//private boolean flush()
    
    private boolean straight()
    {
    	int lowCard = cards[0].getRank(); //Hand is already by the constructor.
    	if(cards[1].getRank() == lowCard+1 && cards[2].getRank() == lowCard+2 && cards[3].getRank() == lowCard+3 && cards[4].getRank() == lowCard+4)
    	{
    		return true;
    	}else if(cards[4].getRank() == 12 && cards[0].getRank() == 0 && cards[1].getRank() == 1 && 
    			cards[2].getRank() == 2 && cards[3].getRank() == 3) {
    		return true;
    	}
    	
    	return false;
    }//private boolean straight()
    
    private ArrayList<ArrayList<Card>> findSortedMatches()
    {
    	 //Find Pairs, 3 of a kind, and 4 of a kind.
    	//String[] ranks = Card.showRanks();  
        ArrayList<ArrayList<Card>> matches = new ArrayList<ArrayList<Card>>();
        ArrayList<Integer> checkedList = new ArrayList<Integer>();
        
        for(int x = 0; x < this.cards.length; x++){
            // increment rank array at the index of each card's rank. 
        	Card card = cards[x];
        	int cardRank = card.getRank();
        	if(!checkedList.contains(cardRank))
        		{
    			checkedList.add(cardRank);
    			ArrayList<Card> tempMatches = new ArrayList<Card>();
    			for(int y = 0; y < cards.length; y++)
    			{
    				if(cards[y].getRank() == card.getRank())
    				{
    					tempMatches.add(cards[y]);
    				}
    			}
    			if(tempMatches.size() > 1)
    			{
    				matches.add(tempMatches);
    			}
        	}
        }
        
        //Bubble Sort to order matches by array length in ascending order.
        for(int x = 0; x < matches.size() - 1; x++) {
        	for(int y = 0; y < matches.size()-1-x; y++) //-x to prevent unnecessary passes.
        	{
        		if(matches.get(y).size() > matches.get(y+1).size())
        		{
        			ArrayList<Card> tempList = matches.get(y);
        			matches.set(y, matches.get(y+1));
        			matches.set(y+1, tempList);
        		}
        	}
        }
        return matches;
	}//private ArrayList<ArrayList<Card>> findSortedMatches()
    
    private void showCardsByRank()
    {
    	System.out.println(cards.length + " cards in hand");
    for (int n = 0; n < cards.length; n++)
        {
            System.out.println(cards[n].rankOfSuit()); //calls array.toString(); 
        }
    }//private void showCardsByRank()
    
    public void gradeHand()
    {
    	ArrayList<ArrayList<Card>> sortedMatches = findSortedMatches();
    	boolean flush = flush();
    	boolean straight = straight();
    	Card nullCard = new Card(-1,-1);
    	//todo ace high?
    	
    	showCardsByRank();
    	if(straight && flush) 
    	{
    		rankMap.put("Straight Flush", cards[cards.length-1]);
    		
    		System.out.println("This hand has a Straight Flush with " + cards[cards.length-1].rankOfSuit());
    	}else rankMap.put("Straight Flush", nullCard);
    	if(sortedMatches.size() > 0 && sortedMatches.get(0).size() == 4)
    	{
    		rankMap.put("Four of a Kind", sortedMatches.get(0).get(0));
    		
    		System.out.println("This hand has a Four of a Kind with " + sortedMatches.get(0).get(0).rankOfSuit());
    	}else rankMap.put("Four of a Kind", nullCard);
    	if(sortedMatches.size() > 1 && sortedMatches.get(0).size() == 3 && sortedMatches.get(1).size() == 2)
    	{
    		rankMap.put("Full House Triplet", sortedMatches.get(0).get(0));
    		rankMap.put("Full House Double", sortedMatches.get(1).get(0));
    		
    		System.out.println("This hand has a Full House with triple " + sortedMatches.get(1).get(0).rankOfSuit()
    		 + " and double " + sortedMatches.get(0).get(0).rankOfSuit());
    	}else {
    		rankMap.put("Full House Triplet", nullCard);
    		rankMap.put("Full House Double", nullCard);
    	}
    	if(flush) {
    		rankMap.put("Flush", cards[cards.length-1]);
    		
    		System.out.println("This hand has a Flush with " + cards[cards.length-1].rankOfSuit());
    	}else rankMap.put("Flush", nullCard);

    	if(straight) {
    		//If there is an Ace in a hand marked Straight, it's being played Aces Low. Grab the next highest value.
    		Card highStraight = cards[cards.length-1];
    		if(highStraight.getRank() == 12) {
    			highStraight = cards[cards.length-2]; //This card's rank should always be "5".
    			}
    		
    		rankMap.put("Straight", highStraight);
    		System.out.println("This hand has a Straight with " + highStraight.rankOfSuit());
    	}else rankMap.put("Straight", nullCard);

    	if(sortedMatches.size() > 0 && sortedMatches.get(0).size() == 3) {//Already filtered out full houses
    		rankMap.put("Three of a Kind", sortedMatches.get(0).get(0));
    		
    		System.out.println("This hand has a Three of a Kind with " + sortedMatches.get(0).get(0).rankOfSuit());
    	}else rankMap.put("Three of a Kind", nullCard);

    	if(sortedMatches.size() > 1 && sortedMatches.get(0).size() == 2 && sortedMatches.get(1).size() == 2){
    		rankMap.put("Two Pairs High", sortedMatches.get(1).get(0));
    		rankMap.put("Two Pairs Low", sortedMatches.get(0).get(0));
    		
    		System.out.println("This hand has Two Pairs with a high of " + Card.rankToString(sortedMatches.get(1).get(0).getRank()) + " and a low of " + Card.rankToString(sortedMatches.get(0).get(0).getRank()));
    	}else {
    		rankMap.put("Two Pairs High", nullCard);
    		rankMap.put("Two Pairs Low", nullCard);
    	}
    	if(sortedMatches.size() > 0 && sortedMatches.get(0).size() == 2) {//Already filtered out Two Pairs
    		rankMap.put("Pair", sortedMatches.get(0).get(0));
    		
    		System.out.println("This hand has one Pair of " + Card.rankToString(sortedMatches.get(0).get(0).getRank()) + "s");
    	}else rankMap.put("Pair", nullCard);
   
    	//No need to track the High Card. It will be evaluated during grading.
    	System.out.println("The High Card for this hand is " + Card.rankToString(cards[cards.length-1].getRank()));
    }//public void gradeHand()
    
    
}//public class Hand 
